import {Component} from '@angular/core';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-datepicker-basic',
  templateUrl: './datepicker-basic.html'
})
export class NgbdDatepickerBasic {

  model: NgbDateStruct;
  date: {year: number, month: number};
  events: any;
  data: any;
  dates: Array<Date>;
  homework: Array<Date>;

  constructor(private calendar: NgbCalendar) {
    this.homework = [];
    this.homework.push(new Date(2019, 10, 8));
    this.homework.push(new Date(2019, 10, 9));
  }

  selectToday() {
    this.model = this.calendar.getToday();
  }

  changed() {
    let date = new Date(`${this.model.year}-${this.model.month}-${this.model.day}`);
    this.dates = [];
    for (let i = 0; i < 7; i++) {
      let newDate = new Date(date);
      newDate.setDate(date.getDate() + i);
      if (this.homework.find(value => {
        console.log("value", value.toDateString());
        console.log("newDate", newDate.toDateString());
        newDate.toDateString();
        return value.toDateString() === newDate.toDateString()}) !== undefined) {
        this.dates.push(newDate)
      }
    }
  }
}
